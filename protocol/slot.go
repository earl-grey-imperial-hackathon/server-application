package protocol

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/slot"
	"strconv"
	"strings"
)

const (
	HeaderDelimiter   = "-"
	InternalDelimiter = "#"
	RowDelimiter      = "/"
	RequestDelimiter  = "###"
)

type SpinRequest struct {
	Bet uint32
}

type SpinResponse struct {
	Result slot.Result
}

func UnpackSpinRequest(cmd string) (SpinRequest, error) {
	parts := strings.Split(cmd, RequestDelimiter)
	if len(parts) != 2 || parts[0] != "spin" {
		return SpinRequest{}, errors.New("invalid spin request")
	}
	bet, err := strconv.ParseUint(parts[1], 10, 32)
	if err != nil {
		return SpinRequest{}, err
	}
	return SpinRequest{Bet: uint32(bet)}, nil
}

func PackSpinResponse(r SpinResponse) string {
	header := fmt.Sprintf("%v#%v#%v", r.Result.RowCount(), r.Result.ColumnCount, r.Result.CreditsNow)
	var rows []string
	for _, row := range r.Result.Rows {
		strRow := strings.Join(row.Symbols, InternalDelimiter)
		rows = append(rows, strRow)
	}
	body := strings.Join(rows, RowDelimiter)
	res := strings.Join([]string{header, body}, HeaderDelimiter)
	log.Debugf(res)
	return res
}
