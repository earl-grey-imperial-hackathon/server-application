package protocol

import (
	"errors"
	"fmt"
	"gorm.io/gorm/utils"
	"strings"
)

const (
	RequestLogin    = "login"
	RequestRegister = "register"
	Delimiter       = "###"
)

type AuthenticationRequest struct {
	Type     string
	Username string
	Password string
}

type AuthenticationResponse struct {
	Ok    bool
	Coins uint32
}

func UnpackAuthenticationRequest(cmd string) (AuthenticationRequest, error) {
	parts := strings.Split(cmd, Delimiter)
	if len(parts) != 3 {
		return AuthenticationRequest{}, errors.New("register###username###password")
	}
	rType := parts[0]
	username := parts[1]
	password := parts[2]
	return AuthenticationRequest{
		Type:     rType,
		Username: username,
		Password: password,
	}, nil
}

func PackAuthenticationResponse(r AuthenticationResponse) string {
	return fmt.Sprintf("%v###%v\n", r.Ok, utils.ToString(r.Coins))
}
