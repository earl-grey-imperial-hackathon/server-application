package slot

func GetConsecutiveIndexSubsets(rowCount uint32, combo WinningCombination) [][]uint32 {
	comboSize := len(combo.Symbols)
	var start uint32 = 0
	var end = uint32(comboSize)
	var indices []uint32
	for i := 0; uint32(i) < rowCount; i++ {
		indices = append(indices, uint32(i))
	}
	var subsets [][]uint32
	for end <= rowCount {
		subset := indices[start:end]
		subsets = append(subsets, subset)
		start++
		end++
	}
	return subsets
}
