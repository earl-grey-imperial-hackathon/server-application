package slot

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/database"
	"gorm.io/gorm"
	"math"
	"math/rand"
	"time"
)

type Slot struct {
	Reels               []Reel `json:"reels"`
	ReelDisplayCount    uint32 // odd number
	WinningCombinations []WinningCombination
}

func (s *Slot) Rows() uint32 {
	return s.ReelDisplayCount
}

func (s *Slot) Columns() uint32 {
	return uint32(len(s.Reels))
}

func (s *Slot) Spin(user database.UserOut, db *gorm.DB, bet uint32) (Result, error) {
	if user.Credits == 0 {
		return Result{}, errors.New("insufficient funds")
	}
	var indices []uint32
	rand.Seed(time.Now().UnixNano())
	for _, reel := range s.Reels {
		index := rand.Intn(len(reel.Symbols))
		indices = append(indices, uint32(index))
	}
	var modifiers []int32
	diff := int32(s.ReelDisplayCount) / 2
	for i := -diff; i <= +diff; i++ {
		modifiers = append(modifiers, i)
	}
	var rows []ResultRow
	for _, modifier := range modifiers {
		var symbols []string
		for i, index := range indices {
			modifiedIndex := int32(index) + modifier
			if modifiedIndex < 0 {
				modifiedIndex += int32(len(s.Reels[i].Symbols))
			} else if modifiedIndex >= int32(len(s.Reels[i].Symbols)) {
				modifiedIndex -= int32(len(s.Reels[i].Symbols))
			}
			symbols = append(symbols, s.Reels[i].Symbols[modifiedIndex])
		}
		rows = append(rows, ResultRow{Symbols: symbols})
	}
	var maxPrize uint32 = 0
	for _, combination := range s.WinningCombinations {
		prize := combination.GetPrize(rows, bet)
		maxPrize = uint32(math.Max(float64(maxPrize), float64(prize)))
		if prize > 0 {
			middleRow := rows[uint32(len(rows))/2]
			log.Infof("WIN!!! Row %v --- Combo: %v", middleRow.Symbols, combination.Symbols)
		}
	}
	userDB := user.GetModel(db)
	currentCredits := userDB.Credits
	var totalCredits = int32(currentCredits) - int32(bet) + int32(maxPrize)
	if totalCredits < 0 {
		totalCredits = 0
		_ = user.SetCredits(uint32(totalCredits), db)
		return Result{}, errors.New("insufficient funds")
	}
	_ = user.SetCredits(uint32(totalCredits), db)
	log.Info("new credits -> ", uint32(totalCredits))
	return Result{
		Rows:        rows,
		CreditsNow:  uint32(totalCredits),
		ColumnCount: s.Columns(),
	}, nil
}

func (s *Slot) SimulateSpin(bet uint32) uint32 {
	var indices []uint32
	rand.Seed(time.Now().UnixNano())
	for _, reel := range s.Reels {
		index := rand.Intn(len(reel.Symbols))
		indices = append(indices, uint32(index))
	}
	var modifiers []int32
	diff := int32(s.ReelDisplayCount) / 2
	for i := -diff; i <= +diff; i++ {
		modifiers = append(modifiers, i)
	}
	var rows []ResultRow
	for _, modifier := range modifiers {
		var symbols []string
		for i, index := range indices {
			modifiedIndex := int32(index) + modifier
			if modifiedIndex < 0 {
				modifiedIndex += int32(len(s.Reels[i].Symbols))
			} else if modifiedIndex >= int32(len(s.Reels[i].Symbols)) {
				modifiedIndex -= int32(len(s.Reels[i].Symbols))
			}
			symbols = append(symbols, s.Reels[i].Symbols[modifiedIndex])
		}
		rows = append(rows, ResultRow{Symbols: symbols})
	}
	var maxPrize uint32 = 0
	for _, combination := range s.WinningCombinations {
		prize := combination.GetPrize(rows, bet)
		maxPrize = uint32(math.Max(float64(maxPrize), float64(prize)))
	}
	return maxPrize
}

type Reel struct {
	Symbols []string `json:"symbols"`
}

type ResultRow struct {
	Symbols []string `json:"symbols"`
}

type Result struct {
	Rows        []ResultRow `json:"rows"`
	CreditsNow  uint32      `json:"creditsWon" `
	ColumnCount uint32      `json:"column_count"`
}

func (r *Result) RowCount() uint32 {
	return uint32(len(r.Rows))
}

func NewSlot(reels []Reel, winningCombinations []WinningCombination) Slot {
	return Slot{
		Reels:               reels,
		ReelDisplayCount:    3,
		WinningCombinations: winningCombinations,
	}
}

func NewReel(symbols []string) Reel {
	return Reel{
		Symbols: symbols,
	}
}

type SymbolWithCount struct {
	Symbol string
	Count  uint32
}

func NewSymbolWithCount(s string, count uint32) SymbolWithCount {
	return SymbolWithCount{
		Symbol: s,
		Count:  count,
	}
}

func SymbolUnpacker(s SymbolWithCount) []string {
	var arr []string
	for i := 0; i < int(s.Count); i++ {
		arr = append(arr, s.Symbol)
	}
	return arr
}

func SymbolArrayUnpacker(arr []SymbolWithCount) []string {
	var result []string
	for _, symbolWithCount := range arr {
		unpacked := SymbolUnpacker(symbolWithCount)
		result = append(result, unpacked...)
	}
	return result
}

func StringArrayShuffle(arr []string) []string {
	rand.Shuffle(len(arr), func(i, j int) {
		arr[i], arr[j] = arr[j], arr[i]
	})
	return arr
}
