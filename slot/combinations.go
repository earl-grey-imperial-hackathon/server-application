package slot

import (
	"math"
)

const (
	Scatter = "K"
)

type WinningCombination struct {
	Symbols []string `json:"symbols"`
	Prize   uint32   `json:"prize"`
}

func (w *WinningCombination) IsScatter() bool {
	for _, symbol := range w.Symbols {
		if symbol != Scatter {
			return false
		}
	}
	return true
}

func NewWinningCombination(symbols []string, prize uint32) WinningCombination {
	return WinningCombination{
		Symbols: symbols,
		Prize:   prize,
	}
}

func (w *WinningCombination) GetPrize(resultRows []ResultRow, bet uint32) uint32 {
	indexSubsets := GetConsecutiveIndexSubsets(uint32(len(resultRows[0].Symbols)), *w)
	var maxReward uint32 = 0

	//non-scatters
	if !w.IsScatter() {
		for _, subset := range indexSubsets {
			row := resultRows[uint32(len(resultRows))/2]
			isWinRow := true
			var count uint32 = 0
			for _, index := range subset {
				if w.Symbols[index-subset[0]] != row.Symbols[index] {
					if row.Symbols[index] != "wildcard" {
						isWinRow = false
						break
					}
				}
				count++
			}
			if count < uint32(len(subset)) {
				isWinRow = false
			}
			if isWinRow {
				maxReward = uint32(math.Max(float64(maxReward), float64(w.Prize*bet)))
			}
		}
	} else {
		//scatters
		rowSize := len(resultRows)
		columnSize := len(resultRows[0].Symbols)
		scatterCount := 0
		for i := 0; i < columnSize; i++ {
			for j := 0; j < rowSize; j++ {
				if resultRows[j].Symbols[i] == Scatter {
					scatterCount++
				}
			}
		}
		if scatterCount == len(w.Symbols) {
			maxReward = uint32(math.Max(float64(maxReward), float64(w.Prize*bet)))
		}
	}

	return maxReward
}
