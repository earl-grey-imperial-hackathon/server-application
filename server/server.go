package server

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/database"
	"gorm.io/gorm"
	"net"
)

type CommandHandler func(string, net.Conn, database.UserOut, *gorm.DB)
type ConnectionHandler func(net.Conn, database.UserOut, *gorm.DB)
type DBConnectionHandler func() (*gorm.DB, error)
type AuthenticationHandler func(net.Conn, *gorm.DB) (database.UserOut, error)

type Server struct {
	AuthHandler   AuthenticationHandler
	ConnHandler   ConnectionHandler
	DBConnHandler DBConnectionHandler
}

func (s *Server) Run() {
	db, err := s.DBConnHandler()
	if err != nil {
		return
	}
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		return
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error("Accept Error", err)
			continue
		}
		user, err := s.AuthHandler(conn, db)
		if err != nil {
			log.Info("Login/Register failed ", err)
		}
		log.Println("Accepted ", conn.RemoteAddr())

		go s.ConnHandler(conn, user, db)
	}
}

func NewServer(connHandler ConnectionHandler, dbConnHandler DBConnectionHandler, defaultAuthHandler AuthenticationHandler) Server {
	return Server{
		ConnHandler:   connHandler,
		DBConnHandler: dbConnHandler,
		AuthHandler:   defaultAuthHandler,
	}
}
