package server

import (
	"bufio"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/authentication"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/database"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/protocol"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/slot"
	"gorm.io/gorm"
	"net"
	"strings"
	"time"
)

var symbolsWithCountR1 = []slot.SymbolWithCount{
	slot.NewSymbolWithCount("banana", 3),
	slot.NewSymbolWithCount("coconut", 2),
	slot.NewSymbolWithCount("wildcard", 1),
	slot.NewSymbolWithCount("K", 1),
	slot.NewSymbolWithCount("mushroom", 3),
	slot.NewSymbolWithCount("peach", 2),
	slot.NewSymbolWithCount("pineapple", 1)}

var symbolsWithCountR2 = []slot.SymbolWithCount{
	slot.NewSymbolWithCount("banana", 2),
	slot.NewSymbolWithCount("coconut", 1),
	slot.NewSymbolWithCount("wildcard", 1),
	slot.NewSymbolWithCount("K", 1),
	slot.NewSymbolWithCount("mushroom", 4),
	slot.NewSymbolWithCount("peach", 1),
	slot.NewSymbolWithCount("pineapple", 3)}

var symbolsWithCountR3 = []slot.SymbolWithCount{
	slot.NewSymbolWithCount("banana", 1),
	slot.NewSymbolWithCount("coconut", 2),
	slot.NewSymbolWithCount("wildcard", 1),
	slot.NewSymbolWithCount("K", 1),
	slot.NewSymbolWithCount("mushroom", 2),
	slot.NewSymbolWithCount("peach", 3),
	slot.NewSymbolWithCount("pineapple", 3)}

var symbolsWithCountR4 = []slot.SymbolWithCount{
	slot.NewSymbolWithCount("banana", 4),
	slot.NewSymbolWithCount("coconut", 1),
	slot.NewSymbolWithCount("wildcard", 1),
	slot.NewSymbolWithCount("K", 1),
	slot.NewSymbolWithCount("mushroom", 2),
	slot.NewSymbolWithCount("peach", 3),
	slot.NewSymbolWithCount("pineapple", 1)}

var symbolsWithCountR5 = []slot.SymbolWithCount{
	slot.NewSymbolWithCount("banana", 1),
	slot.NewSymbolWithCount("coconut", 1),
	slot.NewSymbolWithCount("wildcard", 1),
	slot.NewSymbolWithCount("K", 1),
	slot.NewSymbolWithCount("mushroom", 3),
	slot.NewSymbolWithCount("peach", 2),
	slot.NewSymbolWithCount("pineapple", 4)}

var symbols1 = slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCountR1))
var symbols2 = slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCountR2))
var symbols3 = slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCountR3))
var symbols4 = slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCountR4))
var symbols5 = slot.StringArrayShuffle(slot.SymbolArrayUnpacker(symbolsWithCountR5))

var reels = []slot.Reel{slot.NewReel(symbols1), slot.NewReel(symbols2), slot.NewReel(symbols3), slot.NewReel(symbols4), slot.NewReel(symbols5)}

var ws1 = []string{"K", "K", "K", "K", "K"}
var ws2 = []string{"coconut", "coconut", "coconut", "coconut", "coconut"}
var ws3 = []string{"mushroom", "mushroom", "mushroom", "mushroom", "mushroom"}
var ws4 = []string{"peach", "peach", "peach", "peach", "peach"}
var ws5 = []string{"pineapple", "pineapple", "pineapple", "pineapple", "pineapple"}
var ws6 = []string{"wildcard", "wildcard", "wildcard", "wildcard", "wildcard"}
var ws7 = []string{"banana", "banana", "banana", "banana", "banana"}

var ws8 = []string{"pineapple", "pineapple", "pineapple", "pineapple"}
var ws9 = []string{"coconut", "coconut", "coconut", "coconut"}
var ws10 = []string{"banana", "banana", "banana", "banana"}
var ws11 = []string{"mushroom", "mushroom", "mushroom", "mushroom"}
var ws12 = []string{"peach", "peach", "peach", "peach"}

var combos = []slot.WinningCombination{slot.NewWinningCombination(ws1, 50),
	slot.NewWinningCombination(ws1, 72),
	slot.NewWinningCombination(ws2, 80),
	slot.NewWinningCombination(ws3, 45),
	slot.NewWinningCombination(ws4, 65),
	slot.NewWinningCombination(ws5, 75),
	slot.NewWinningCombination(ws6, 100),
	slot.NewWinningCombination(ws7, 65),
	slot.NewWinningCombination(ws8, 35),
	slot.NewWinningCombination(ws9, 35),
	slot.NewWinningCombination(ws10, 25),
	slot.NewWinningCombination(ws11, 15),
	slot.NewWinningCombination(ws12, 25)}

var hardcodedSlot = slot.NewSlot(reels, combos)

func DefaultAuthHandler(conn net.Conn, db *gorm.DB) (database.UserOut, error) {
	s := bufio.NewScanner(conn)
	s.Scan()
	rawReq := s.Text()
	r, err := protocol.UnpackAuthenticationRequest(rawReq)
	errorResponse := protocol.AuthenticationResponse{
		Ok:    false,
		Coins: 0,
	}
	if err != nil {
		conn.Write([]byte(protocol.PackAuthenticationResponse(errorResponse)))
		log.Error("Connection closed - unauthorized ", conn.RemoteAddr())
		conn.Close()
		return database.UserOut{}, err
	}
	var user database.UserOut
	if r.Type == protocol.RequestLogin {
		t := time.Now()
		user, err = authentication.Login(r.Username, r.Password, db)
		execTime := time.Since(t)
		log.Infof("Login execution time %vms", execTime.Milliseconds())
	} else if r.Type == protocol.RequestRegister {
		t := time.Now()
		user, err = authentication.Register(r.Username, r.Password, db)
		execTime := time.Since(t)
		log.Infof("Register execution time %vms", execTime.Milliseconds())
	} else {
		conn.Write([]byte(protocol.PackAuthenticationResponse(errorResponse)))
		log.Error("Connection closed - unauthorized ", conn.RemoteAddr())
		conn.Close()
		return database.UserOut{}, errors.New("unsupported type")
	}
	if err != nil {
		conn.Write([]byte(protocol.PackAuthenticationResponse(errorResponse)))
		log.Error("Connection closed - unauthorized ", conn.RemoteAddr())
		conn.Close()
		return database.UserOut{}, err
	}
	successResponse := protocol.AuthenticationResponse{
		Ok:    true,
		Coins: user.Credits,
	}
	conn.Write([]byte(protocol.PackAuthenticationResponse(successResponse)))
	return user, nil
}

func DefaultDBConnHandler() (*gorm.DB, error) {
	var err error
	db, err := database.NewDatabase()
	if err != nil {
		log.Printf("err ", err)
	}
	err = database.Migrate(db)
	if err != nil {
		log.Printf("err ", err)
	}
	return db, nil
}

func DefaultCommandHandler(cmd string, conn net.Conn, user database.UserOut, db *gorm.DB) {
	if strings.Contains(cmd, "spin") {
		req, err := protocol.UnpackSpinRequest(cmd)
		if err != nil {
			log.Error("error ", err)
			conn.Write([]byte(err.Error()))
			return
		}
		log.Info("spin request", " user ", user.Username)
		t := time.Now()
		result, err := hardcodedSlot.Spin(user, db, req.Bet)
		execTime := time.Since(t)
		log.Infof("Spin execution time %vms", execTime.Milliseconds())
		if err != nil {
			conn.Write([]byte(err.Error()))
			conn.Close()
			return
		}
		response := protocol.SpinResponse{Result: result}
		res := protocol.PackSpinResponse(response)
		conn.Write([]byte(fmt.Sprintf("%s\n", res)))
	}
}

func SingleChannelConnectionHandler(conn net.Conn, user database.UserOut, db *gorm.DB) {
	defer conn.Close()

	s := bufio.NewScanner(conn)

	for s.Scan() {

		data := s.Text()

		DefaultCommandHandler(data, conn, user, db)
	}
}
