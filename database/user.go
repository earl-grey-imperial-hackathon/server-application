package database

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID       uint   `gorm:"primaryKey;autoIncrement" json:"id,omitempty"`
	Username string `gorm:"not null;unique" json:"username"`
	Password string `gorm:"not null"  json:"password"`
	Credits  uint32 `gorm:"not null" json:"credits"`
}

func (u *User) Out() UserOut {
	if u.Username == "" {
		var emptyUser UserOut
		return emptyUser
	}
	userOut := UserOut{
		ID:       u.ID,
		Username: u.Username,
		Credits:  u.Credits,
	}
	return userOut
}

type UserIn struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserOut struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Credits  uint32 `json:"credits"`
}

func (o *UserOut) GetModel(db *gorm.DB) User {
	var userDB User
	err := db.Where("id = ?", o.ID).First(&userDB).Error
	if err != nil {
		return User{}
	}
	return userDB
}

func (u *User) SetCredits(credits uint32, db *gorm.DB) error {
	u.Credits = credits
	err := db.Save(&u).Error
	if err != nil {
		return err
	}
	return nil
}

func (o *UserOut) SetCredits(credits uint32, db *gorm.DB) error {
	model := o.GetModel(db)
	return model.SetCredits(credits, db)
}

func UserArrayOut(models []User) []UserOut {
	outArr := make([]UserOut, len(models))
	for i, item := range models {
		outArr[i] = item.Out()
	}
	return outArr
}
