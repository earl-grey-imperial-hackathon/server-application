package authentication

import (
	"errors"
	"gitlab.com/earl-grey-imperial-hackathon/server-application/database"
	"gorm.io/gorm"
)

func Login(username string, password string, db *gorm.DB) (database.UserOut, error) {
	var userDB database.User
	err := db.Where("username = ?", username).First(&userDB).Error
	if err != nil {
		return database.UserOut{}, err
	}
	if !CheckPasswordHash(password, userDB.Password) {
		return database.UserOut{}, errors.New("login failed")
	}
	return userDB.Out(), nil
}

func Register(username string, password string, db *gorm.DB) (database.UserOut, error) {
	hash, err := HashPassword(password)
	if err != nil {
		return database.UserOut{}, err
	}
	dbUser := database.User{
		Username: username,
		Password: hash,
		Credits:  1000,
	}
	result := db.Create(&dbUser)
	if result.Error != nil {
		return database.UserOut{}, result.Error
	}
	return dbUser.Out(), nil
}
