package main

import "gitlab.com/earl-grey-imperial-hackathon/server-application/server"

func main() {
	tcpServer := server.NewServer(server.SingleChannelConnectionHandler, server.DefaultDBConnHandler, server.DefaultAuthHandler)
	tcpServer.Run()
}
